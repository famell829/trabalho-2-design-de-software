Fabiano - GRR20171612
Victor Rocha de Abreu - GRR20171623

Trabalho de DS 2019/2 de adicionar um mural a uma API do ensalamento, desenvolvido pela dupla em loopback com banco de dados local.

Foi implementado uma unica classe chamada Mural a qual tambem esta definida no Diagrama com status de "Não verificados", ela contem como atributos Mensagem, Data e Validade respectivamente: String, Date e Boolean onde todos eles são obrigatorios.

Alem disso a classe tem relação com Turma, Sala, Orgão, Secretario e Professor, somente com Orgão sendo obrigatorio, a classe necessita ter Secretario ou Professor, nao soubemos representar este "OU" entao deixamos 0..1 nas duas relações, sala e Turma são não obrigatorios.

Sem interface grafica somente chamadas diretas na API implementada, quando implementado as funções CRUD foram automaticamente criadas.